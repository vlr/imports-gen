// warning! This code was generated by @vlr/razor
// manual changes to this file will be overwritten next time the code is regenerated.

import { Generator, GeneratorOptions } from "@vlr/razor/export";
import { CompactImport } from "./compact/compactImport.type";

function generateContent(imp: CompactImport, gen: Generator): Generator {
  const indent = gen.indent;
  gen = gen.append(`import {`);
  gen = gen.eol();
  for (let type of imp.types) {
    gen = gen.append(`  `);
    gen = gen.append((type).toString());
    gen = gen.append(`,`);
    gen = gen.eol();
  }
  gen = gen.append(`} from `);
  gen = gen.apostrophe();
  gen = gen.append((imp.importPath).toString());
  gen = gen.apostrophe();
  gen = gen.append(`;`);
  gen = gen.eol();
  gen = gen.setIndent(indent);
  return gen;
}

function generate(imp: CompactImport, options: GeneratorOptions): string {
  return generateContent(imp, new Generator(options)).toString();
}

export const multiLine = {
  generate,
  generateContent
};
