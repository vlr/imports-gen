import { Import, } from "../import.type";
import { CompactImport } from "./compactImport.type";
import { groupBy } from "@vlr/map-tools";
import { flatMap } from "@vlr/array-tools";

const maxLength = 120;

export function squashImports(imports: Import[]): CompactImport[] {
  const grouped = groupBy(imports, imp => imp.importPath);
  return flatMap(grouped.values(), getGroupImports);
}

function getGroupImports(imports: Import[]): CompactImport[] {
  const type = imports.map(joinType).join(", ");
  const importPath = imports[0].importPath;
  const isMultiline = importPath.length + type.length > maxLength;
  return [
    {
      isMultiline,
      importPath,
      types: [type]
    }
  ];
}

function joinType(imp: Import): string {
  return imp.type === imp.alias
    ? imp.alias
    : `${imp.type} as ${imp.alias}`;
}
