export interface CompactImport {
  isMultiline: boolean;
  importPath: string;
  types: string[];
}
