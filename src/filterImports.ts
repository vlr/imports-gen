import { Import } from "./import.type";
import { toMap } from "@vlr/map-tools";
import { unique, flatMap } from "@vlr/array-tools";

type ImportsMap = Map<string, Import>;

export function filterImports(imports: Import[], types: string[]): Import[] {
  const map = toMap(imports, imp => imp.alias);
  return unique(flatMap(types, type => getTypeImports(type, map)));
}

const splitter = /[<>,\s\[\]]/;
function getTypeImports(typeName: string, imports: ImportsMap): Import[] {
  const parts = typeName.split(splitter)
    .map(part => part.trim());
  return unique(parts)
    .filter(part => imports.has(part))
    .map(part => imports.get(part));
}
