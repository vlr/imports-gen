export interface Import {
    type: string;
    alias: string;
    importPath: string;
}
