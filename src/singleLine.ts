// warning! This code was generated by @vlr/razor
// manual changes to this file will be overwritten next time the code is regenerated.

import { Generator, GeneratorOptions } from "@vlr/razor/export";
import { CompactImport } from "./compact/compactImport.type";

function generateContent(imp: CompactImport, gen: Generator): Generator {
  const indent = gen.indent;
  gen = gen.append(`import { `);
  gen = gen.append((imp.types[0]).toString());
  gen = gen.append(` } from `);
  gen = gen.apostrophe();
  gen = gen.append((imp.importPath).toString());
  gen = gen.apostrophe();
  gen = gen.append(`;`);
  gen = gen.eol();
  gen = gen.setIndent(indent);
  return gen;
}

function generate(imp: CompactImport, options: GeneratorOptions): string {
  return generateContent(imp, new Generator(options)).toString();
}

export const singleLine = {
  generate,
  generateContent
};
