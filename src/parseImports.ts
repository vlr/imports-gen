import { Import } from "./import.type";
import { regexToArray, flatMap } from "@vlr/array-tools";

const importRegex = /import\s\s*{([^}]*)}\s\s*from\s\s*["|']([^"']*)["|']/gs;
export function parseImports(content: string): Import[] {
  const matches = regexToArray(importRegex, content);
  return flatMap(matches, match => parseMatch(match[1], match[2]));
}

function parseMatch(types: string, importPath: string): Import[] {
  return types.split(",").map(type => createImport(type, importPath)).filter(x => x);
}

function createImport(type: string, importPath: string): Import {
  const parts = type.split(/\s[\s]*/gs).filter(s => s.length);
  if (!parts.length) { return null; }
  const index = parts.length === 3 && parts[1] === "as" ? 2 : 0;
  return {
    type: parts[0],
    alias: parts[index],
    importPath
  };
}
