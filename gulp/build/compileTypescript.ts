import { spawn } from "../tools/spawn";

export function compileTypescript(): Promise<void> {
  return spawn("compileTypescript", "tsc");
}

export function compileTypescriptEs5(): Promise<void> {
  return spawn("compileTypescriptEs5", "tsc", ["--build", "tsconfig.es5.json"]);
}

export function compileTypescriptEs6(): Promise<void> {
  return spawn("compileTypescriptEs6", "tsc", ["--build", "tsconfig.es6.json"]);
}
