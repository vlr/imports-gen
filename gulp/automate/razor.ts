import * as gulp from "gulp";
import { settings } from "../settings";
import { transform } from "@vlr/gulp-transform";
import { generate } from "@vlr/razor";
import { RazorOptions } from "@vlr/razor";

export function razor(): NodeJS.ReadWriteStream {
  const src = "./" + settings.src;
  const razorFiles = src + "/**/*.rzr";
  const options: RazorOptions = {
    quotes: '"',
    linefeed: "\n",
    language: "ts"
  };
  return gulp.src(razorFiles)
    .pipe(transform(file => ({ ...file, contents: generate(file.contents, options), extension: ".ts" })))
    .pipe(gulp.dest(src));
}
