export const constants = {
  allTs: "**/*.ts",
  allJs: "**/*.js",
  allFiles: "**/*.*"
};
