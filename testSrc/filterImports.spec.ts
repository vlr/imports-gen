import { filterImports } from "../src";
import { Import } from "../src";
import { expect } from "chai";
import { areEquivalent } from "@vlr/array-tools";

describe("filterImports", function (): void {
  it("should include only imports mentioned in types", async function (): Promise<void> {
    // arrange
    const imports: Import[] = [
      { type: "Type1", alias: "Type1", importPath: "." },
      { type: "Type2", alias: "Type2", importPath: "./" },
      { type: "Type3", alias: "Type3", importPath: "./" }
    ];
    const types = ["Type1", "Type2"];

    const expected = [imports[0], imports[1]];

    // act
    const result = filterImports(imports, types);

    // assert
    expect(areEquivalent(expected, result)).equals(true);
  });

  it("should include types once", async function (): Promise<void> {
    // arrange
    const imports: Import[] = [
      { type: "Type1", alias: "Type1", importPath: "." },
      { type: "Type2", alias: "Type2", importPath: "./" }
    ];
    const types = ["Type1", "Type1"];

    const expected = [imports[0]];

    // act
    const result = filterImports(imports, types);

    // assert
    expect(areEquivalent(expected, result)).equals(true);
  });

  it("should ignore non imported types", async function (): Promise<void> {
    // arrange
    const imports: Import[] = [
      { type: "Type1", alias: "Type1", importPath: "." },
      { type: "Type2", alias: "Type2", importPath: "./" }
    ];
    const types = ["Type2", "string", "boolean"];

    const expected = [imports[1]];

    // act
    const result = filterImports(imports, types);

    // assert
    expect(areEquivalent(expected, result)).equals(true);
  });

});
