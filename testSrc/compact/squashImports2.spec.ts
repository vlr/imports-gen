import { Import } from "../../src/import.type";
import { squashImports } from "../../src/compact/squashImports";
import { CompactImport } from "../../src/compact/compactImport.type";
import { checkCompactImports } from "./checkCompactImports";

describe("squashImports", function (): void {
  it("should group and join single imports", async function (): Promise<void> {
    // arrange
    const input: Import[] = [
      { type: "Type1", alias: "Type2", importPath: "./dir1" },
      { type: "Type3", alias: "Type4", importPath: "./dir2" },
      { type: "Type5", alias: "Type6", importPath: "./dir1" },
      { type: "Type7", alias: "Type8", importPath: "./dir2" }
    ];
    const expected: CompactImport[] = [
      { isMultiline: false, types: ["Type1 as Type2, Type5 as Type6"], importPath: "./dir1" },
      { isMultiline: false, types: ["Type3 as Type4, Type7 as Type8"], importPath: "./dir2" }
    ];

    // act
    const result = squashImports(input);

    // assert
    checkCompactImports(result, expected);
  });

  it("should not split single imports when length less than 120", async function (): Promise<void> {
    // arrange
    const input: Import[] = [
      {
        type: "Type10123456789",
        alias: "Type10123456789",
        importPath: "./dir10123456789012345678901234567890123456789"
      },
      {
        type: "Type20123456789",
        alias: "Type20123456789",
        importPath: "./dir10123456789012345678901234567890123456789"
      },
      {
        type: "Type30123456789",
        alias: "Type30123456789",
        importPath: "./dir10123456789012345678901234567890123456789"
      },
      {
        type: "Type40123456789",
        alias: "Type40123456789",
        importPath: "./dir10123456789012345678901234567890123456789"
      },
    ];
    const expected: CompactImport[] = [
      {
        isMultiline: false,
        types: ["Type10123456789, Type20123456789, Type30123456789, Type40123456789"],
        importPath: "./dir10123456789012345678901234567890123456789"
      },
    ];

    // act
    const result = squashImports(input);

    // assert
    checkCompactImports(result, expected);
  });

  // it("should split imports in 2 lines if length is more than 120", async function (): Promise<void> {
  //   // arrange
  //   const input: Import[] = [
  //     {
  //       type: "Type10123456789",
  //       alias: "Type10123456789",
  //       importPath: "./dir10123456789012345678901234567890123456789"
  //     },
  //     {
  //       type: "Type20123456789",
  //       alias: "Type20123456789",
  //       importPath: "./dir10123456789012345678901234567890123456789"
  //     },
  //     {
  //       type: "Type30123456789",
  //       alias: "Type30123456789",
  //       importPath: "./dir10123456789012345678901234567890123456789"
  //     },
  //     {
  //       type: "Type40123456789",
  //       alias: "Type40123456789",
  //       importPath: "./dir10123456789012345678901234567890123456789"
  //     },
  //     {
  //       type: "Type50123456789",
  //       alias: "Type50123456789",
  //       importPath: "./dir10123456789012345678901234567890123456789"
  //     },
  //   ];
  //   const expected: CompactImport[] = [
  //     {
  //       isMultiline: false,
  //       types: ["Type10123456789, Type20123456789, Type30123456789, Type40123456789"],
  //       importPath: "./dir10123456789012345678901234567890123456789"
  //     },
  //     {
  //       isMultiline: false,
  //       types: ["Type50123456789"],
  //       importPath: "./dir10123456789012345678901234567890123456789"
  //     },
  //   ];

  //   // act
  //   const result = squashImports(input);

  //   // assert
  //   checkCompactImports(result, expected);
  // });

  // it("should produce multiline if there are more types than 2 single imports can hold", async function (): Promise<void> {
  //   // arrange
  //   const input: Import[] = [
  //     {
  //       type: "Type10123456789",
  //       alias: "Type10123456789",
  //       importPath: "./dir10123456789012345678901234567890123456789"
  //     },
  //     {
  //       type: "Type20123456789",
  //       alias: "Type20123456789",
  //       importPath: "./dir10123456789012345678901234567890123456789"
  //     },
  //     {
  //       type: "Type30123456789",
  //       alias: "Type30123456789",
  //       importPath: "./dir10123456789012345678901234567890123456789"
  //     },
  //     {
  //       type: "Type40123456789",
  //       alias: "Type40123456789",
  //       importPath: "./dir10123456789012345678901234567890123456789"
  //     },
  //     {
  //       type: "Type50123456789",
  //       alias: "Type50123456789",
  //       importPath: "./dir10123456789012345678901234567890123456789"
  //     },
  //     {
  //       type: "Type60123456789",
  //       alias: "Type60123456789",
  //       importPath: "./dir10123456789012345678901234567890123456789"
  //     },
  //     {
  //       type: "Type70123456789",
  //       alias: "Type70123456789",
  //       importPath: "./dir10123456789012345678901234567890123456789"
  //     },
  //     {
  //       type: "Type80123456789",
  //       alias: "Type80123456789",
  //       importPath: "./dir10123456789012345678901234567890123456789"
  //     },
  //     {
  //       type: "Type90123456789",
  //       alias: "Type90123456789",
  //       importPath: "./dir10123456789012345678901234567890123456789"
  //     },
  //   ];
  //   const expected: CompactImport[] = [
  //     {
  //       isMultiline: true,
  //       types: [
  //         "Type10123456789, Type20123456789, Type30123456789, Type40123456789, Type50123456789, Type60123456789, Type70123456789",
  //         "Type80123456789, Type90123456789"
  //       ],
  //       importPath: "./dir10123456789012345678901234567890123456789"
  //     }
  //   ];

  //   // act
  //   const result = squashImports(input);

  //   // assert
  //   checkCompactImports(result, expected);
  // });
});


