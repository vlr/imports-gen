import { expect } from "chai";
import { CompactImport } from "../../src/compact/compactImport.type";
import { areEquivalent } from "@vlr/array-tools";

export function checkCompactImports(result: CompactImport[], ex: CompactImport[]): void {
  expect(result.length).equals(ex.length);
  ex.forEach(e => checkPair(e, result));
}

function checkPair(ex: CompactImport, result: CompactImport[]): void {
  const pair = result.filter(r => r.importPath === ex.importPath)
    .filter(r => r.isMultiline === ex.isMultiline)
    .find(r => areEquivalent(r.types, ex.types));
  expect(pair != null).equals(true, "result does not contain expected import");
}
