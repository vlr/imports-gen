import { CompactImport } from "../../src/compact/compactImport.type";
import { squashImports } from "../../src/compact/squashImports";
import { Import } from "../../src/import.type";
import { checkCompactImports } from "./checkCompactImports";

describe("squashImports", function (): void {
  it("should work with single import", async function (): Promise<void> {
    // arrange
    const input: Import[] = [
      { type: "Type1", alias: "Type1", importPath: "." }
    ];
    const expected: CompactImport[] = [
      { isMultiline: false, types: ["Type1"], importPath: "." }
    ];

    // act
    const result = squashImports(input);

    // assert
    checkCompactImports(result, expected);
  });

  it("should work with aliased import", async function (): Promise<void> {
    // arrange
    const input: Import[] = [
      { type: "Type1", alias: "Type2", importPath: "." }
    ];
    const expected: CompactImport[] = [
      { isMultiline: false, types: ["Type1 as Type2"], importPath: "." }
    ];

    // act
    const result = squashImports(input);

    // assert
    checkCompactImports(result, expected);
  });

  it("should not produce multiline if types + path is less than 120", async function (): Promise<void> {
    // arrange
    const input: Import[] = [
      {
        type: "Type10123456789012345678901234567890123456789",
        alias: "Type20123456789012345678901234567890123456789",
        importPath: "./dir01234567890123456789"
      }
    ];
    const expected: CompactImport[] = [
      {
        isMultiline: false,
        types: ["Type10123456789012345678901234567890123456789 as Type20123456789012345678901234567890123456789"],
        importPath: "./dir01234567890123456789"
      }
    ];

    // act
    const result = squashImports(input);

    // assert
    checkCompactImports(result, expected);
  });

  it("should produce multiline if types + path is longer than 120", async function (): Promise<void> {
    // arrange
    const input: Import[] = [
      {
        type: "Type10123456789012345678901234567890123456789",
        alias: "Type20123456789012345678901234567890123456789",
        importPath: "./dir0123456789012345678901234567890123456789"
      }
    ];
    const expected: CompactImport[] = [
      {
        isMultiline: true,
        types: ["Type10123456789012345678901234567890123456789 as Type20123456789012345678901234567890123456789"],
        importPath: "./dir0123456789012345678901234567890123456789"
      }
    ];

    // act
    const result = squashImports(input);

    // assert
    checkCompactImports(result, expected);
  });
});


